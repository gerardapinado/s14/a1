console.log("Hello World")
let user = {
	firstname: "John",
	lastname: "Smith",
	age: 30,
	isMarried: false,
	hobbies: ["Biking", "Mountain Climbing", "Swimming"],
	workAddress: {
		houseNumber: "32",
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	}
}

// Function for printing user info that accept first, last name, and age
// Function for printing user hobbies and work address
function printUserInfo(uFname, uLname, uAge){
	// Print user first, last name, and age
	console.log(uFname +" "+ uLname + " is " + uAge + " years of age.")
	console.log("This was printed inside of the function")
	// Print user hobbies and work address
	console.log(user.hobbies)
	console.log("This was printed inside of the function")
	console.log("Work Address:")
	console.log(user.workAddress)
}

// Function that returns true boolean value
function returnFunction(){
	return true
}

// Print all user information
console.log("First Name: "+user.firstname)
console.log("Last Name: "+user.lastname)
console.log("Age: "+user.age)
console.log("Hobbies:")
console.log(user.hobbies)
console.log("Work Address:")
console.log(user.workAddress)

// function call for printUserInfo
printUserInfo(user.firstname, user.lastname, user.age)

// reassign user.isMarried value using returnFunction()
user.isMarried = returnFunction();
console.log("The value of isMarried is: " + user.isMarried)